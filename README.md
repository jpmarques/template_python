# Nome do Projeto

> Lorem IpsumLorem IpsumLorem IpsumLorem IpsuLorem IpsumLorem IpsuLorem IpsuLorem Ipsum

### Ajustes e melhorias

- [ ] Feature 1.
- [ ] Feature 2.
- [ ] Feature 3.
- [ ] Feature 4.

## 📫 Contribuindo

Para contribuir com simple HTTP, siga estas etapas:

1. Bifurque este repositório.
2. Crie um branch: `git checkout -b <nome_branch>`.
3. Faça suas alterações e confirme-as: `git commit -m '<mensagem_commit>'`
4. Envie para o branch original: `git push origin <nome_do_projeto> / <local>`
5. Crie a solicitação de pull.

Como alternativa, consulte a documentação do Codeberg em [como criar uma solicitação pull](https://docs.codeberg.org/collaborating/pull-requests-and-git-flow/).

## 🤝 Colaboradores

Agradecemos às seguintes pessoas que contribuíram para este projeto:

<table>
  <tr>
    <td align="center">
      <a href="https://codeberg.org/jpmarques" title="perfil do codeberg">
        <img src="https://codeberg.org/avatars/8e446c80eb92f6e8db21b516df225d174cd1dab3d812b4a9fdb27926b329bde0?size=512" width="100px;" alt="Foto do Joao no Codeberg"/><br>
        <sub>
          <b>jpmarques</b>
        </sub>
      </a>
    </td>
  </tr>
</table>
<!--
## 😄 Seja um dos contribuidores

Quer fazer parte desse projeto? Clique [AQUI](CONTRIBUTING.md) e leia como contribuir.
-->
## 📝 Licença

Esse projeto está sob licença. Veja o arquivo [LICENÇA](LICENSE.md) para mais detalhes.

